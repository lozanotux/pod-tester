# POD-TESTER

This `Python 3` application is for test pods inside kubernetes or openshift clusters.

![snapshot](./static/assets/snapshot.png)

## **how to use?**

You can run this app locally (Python 3 required), executing the next command:
```bash
$ pip install -r requirements.txt
$ python3 app.py
```

You can also build a Docker image, just run the next command:
```bash
$ docker build -t pod-tester:latest .
```

To run this app trought a docker image, execute the next command:
```bash
$ docker run --rm --name pod-tester -e VAR1=mensaje pod-tester:latest
```

> **NOTE:** this app support a `VAR1` environment variable to change it with a **ConfigMap** or **Secret** object.