from urllib import request
from flask import Flask, render_template, url_for
from waitress import serve
from os import environ
from datetime import datetime
import secrets

SECRET_KEY = secrets.token_hex()
http_requests = 0

app = Flask(__name__)

@app.route('/')
@app.route('/index')
def home():
    hostname = environ.get('HOSTNAME', 'N/A')
    var1 = environ.get('VAR1', 'N/A')
    global http_requests
    http_requests += 1
    print('{} "{}" {}: receiving a request (#{}) --> Values(pod-name[{}], var1[{}])'.format(datetime.now(), "GET", url_for('home'), http_requests, hostname, var1))
    return render_template('index.html', hostname=hostname, var1=var1)

if __name__ == '__main__':
    app.secret_key = SECRET_KEY
    print("")
    print("#############################")
    print("### Welcome to POD-TESTER ###")
    print("#############################")
    print("")
    print(" * Running on port 8080")
    print("")
    print("--> Logs & Invocations:")
    print("")
    serve(app, host='0.0.0.0', port=8080)